var CLICKED = false;
const AVAILABLE_URLS = ["https://norber.m4u/WebPonto/", "https://norber.m4u/WebPonto/menu/main.asp"];
const norberUrl = "https://norber.m4u/WebPonto/";
const mountCode = (usr, pwd, company) => `
    const unsafeUrlBtn = document.querySelector("#details-button");
    if (unsafeUrlBtn !== null) {
        unsafeUrlBtn.click();
        document.querySelector("#proceed-link").click();
    }
    
    if (document.querySelector("#requiredusuario")) {
        const companyInput = document.querySelector("#CodEmpresa");
        companyInput.value = "${company}"
        companyInput.dispatchEvent(new Event("blur"));

        const userInput = document.querySelector("#requiredusuario");
        userInput.value = "${usr}";
        userInput.dispatchEvent(new Event("input"));

        const pwdInput = document.querySelector("#requiredsenha");
        pwdInput.value = "${pwd}";
        pwdInput.dispatchEvent(new Event("input"));

        document.querySelector("input[type=submit]").click();
    }
`
const validateSettings = (callback) => {
    chrome.storage.sync.get({
        username: null,
        pwd: null,
        checkIn: false,
        company: 1
    }, ({ username, pwd, checkIn, company }) => callback(username, pwd, checkIn, company));
}

chrome.browserAction.onClicked.addListener(function (_) {
    CLICKED = true;
    validateSettings((username, _pwd, _checkIn) => {
        if (!username) {
            alert("Por favor configure seu usuário e senha! :)");
            chrome.tabs.create({ 'url': `chrome-extension://${chrome.runtime.id}/options.html` });
        } else {
            chrome.tabs.create({ url: norberUrl });
        }
    });
});

chrome.tabs.onUpdated.addListener(function (tabId, changeInfo, tab) {
    if (changeInfo.status === 'complete' && AVAILABLE_URLS.includes(tab.url)) {
        validateSettings((username, pwd, checkIn, company) => {
            if (tab.url === "https://norber.m4u/WebPonto/menu/main.asp") {
                if (checkIn) {
                    chrome.tabs.executeScript(
                        tabId,
                        { code: 'document.querySelector("#menu2_Item2").click()' }
                    );
                }
            } else if (tab.url === "https://norber.m4u/WebPonto/" && CLICKED) {
                CLICKED = false;
                const code = mountCode(username, pwd, company);
                chrome.tabs.executeScript(
                    tabId,
                    { code }
                );
            }
        });
    }
});