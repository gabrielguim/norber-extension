function saveOptions() {
    const username = document.getElementById('name').value;
    const pwd = document.getElementById('pwd').value;
    const company = document.getElementById('codigo_empresa').value;
    const checkIn = document.getElementById('marcacao_ponto').checked;

    chrome.storage.sync.set({ username, pwd, checkIn, company }, () => {
        if (confirm("Salvo!\n\nDeseja prosseguir para o login?")) {
            chrome.browserAction.openPopup(() => {});
        }
    });
}

function fetchOptions() {
    // Use default value color = 'red' and likesColor = true.
    chrome.storage.sync.get({
        username: 'seu_usuario',
        pwd: 'xpto',
        checkIn: false,
        company: 1
    }, function (items) {
        document.getElementById('codigo_empresa').value = items.company;
        document.getElementById('name').value = items.username;
        document.getElementById('pwd').value = items.pwd;
        document.getElementById('marcacao_ponto').checked = items.checkIn;
    });
}

document.addEventListener('DOMContentLoaded', fetchOptions);
document.getElementById('save').addEventListener('click', saveOptions);