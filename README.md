# README #

Um projetinho simples para resolver um problema simples de preguiça para copiar minha senha automática do Norber toda vez que eu preciso utilizar o sistema.

### Como instalar? ###

* [Baixe](https://bitbucket.org/gabrielguim/norber-extension/get/master.zip) ou clone o Repositório
* Extraia o arquivo comprimido em qualquer lugar
* Abra o Chrome Extensions (copie e cole o __chrome://extensions/__ no seu navegador)
* Ative o modo desenvolvedor (No toggle do lado superior direito)
* Clique em __"Carregar sem compactação"__
* E selecione toda a pasta extraída
* Pronto. Agora basta clicar no ícone da extensão e ele automaticamente fará o login para você! :)

### GIFzinho camarada ###
![](https://bitbucket.org/gabrielguim/norber-extension/raw/a92d8c50462b01cbc48bb5e1250285c8d408b2ff/norber.gif)